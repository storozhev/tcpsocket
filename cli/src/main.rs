use rw::{read_from, write_to};
use std::io;
use std::net::TcpStream;

fn main() -> io::Result<()> {
    let stream = TcpStream::connect("127.0.0.1:8080").expect("failed to connect");

    println!("{}", read_from(&stream)?);

    loop {
        let mut buf = String::new();
        io::stdin().read_line(&mut buf)?;

        let cmd = match buf.trim_end() {
            "" => continue,
            cmd => cmd,
        };

        write_to(&stream, cmd)?;

        match read_from(&stream) {
            Ok(result) => match result.as_str() {
                "quit" => break,
                result => println!("{}", result),
            },
            Err(_) => break,
        }
    }

    Ok(())
}

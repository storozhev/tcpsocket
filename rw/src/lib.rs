use std::io::{self, Read, Write};

pub fn write_to<W: Write, T: AsRef<str>>(mut writer: W, content: T) -> io::Result<()> {
    let buf = content.as_ref().as_bytes();
    let buf_len = buf.len() as u32;
    let full_buf = [&buf_len.to_be_bytes(), buf].concat();

    writer.write_all(&full_buf)
}

pub fn read_from<R: Read>(mut reader: R) -> io::Result<String> {
    let mut buf = [0u8; 4];
    reader.read_exact(&mut buf)?;

    let buf_len = u32::from_be_bytes(buf);
    let mut buf = vec![0; buf_len as _];

    reader.read_exact(&mut buf)?;

    Ok(String::from_utf8(buf).unwrap())
}

#[cfg(test)]
mod test {
    use super::{read_from, write_to};

    #[test]
    fn read_from_success() {
        let data = "hello world";
        let data_len = data.len() as u32;
        let buf = [&data_len.to_be_bytes(), data.as_bytes()].concat();

        let result = read_from(&buf[..]);

        assert!(result.is_ok());
        assert_eq!("hello world".to_string(), result.unwrap());
    }

    #[test]
    fn write_to_success() {
        let mut buf: Vec<u8> = Vec::new();
        let result = write_to(&mut buf, "hello world");

        assert!(result.is_ok());
        assert_eq!(vec![0, 0, 0, 11], buf[..4]);
        assert_eq!(Vec::from("hello world"), buf[4..]);
    }
}

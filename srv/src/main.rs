use std::{
    error::Error,
    io::Read,
    net::{TcpListener, TcpStream},
    sync::Arc,
    sync::Mutex,
    thread,
};

use rw::{read_from, write_to};

mod command;
mod socket;
use socket::Socket;

use crate::command::{commands, Command, CommandError};

fn main() {
    let listener = TcpListener::bind("127.0.0.1:8080").expect("could not bind addr: ..");
    let socket = Arc::new(Mutex::new(Socket::default()));

    for stream in listener.incoming() {
        if let Err(e) = stream {
            println!("connection failed: {}", e);
            continue;
        }

        let stream = stream.unwrap();
        let socket_clone = Arc::clone(&socket);

        thread::spawn(move || {
            handle_connection(stream, socket_clone);
        });
    }
}

fn handle_connection(stream: TcpStream, arc_mrx_socket: Arc<Mutex<Socket>>) {
    println!("incoming connection: {:?}", stream.peer_addr());

    write_to(&stream, commands()).unwrap();

    while match get_command(&stream) {
        Ok(cmd) => {
            let mut socket = arc_mrx_socket.lock().unwrap();
            write_to(&stream, cmd.execute(&mut socket)).is_ok()
        }
        Err(e) => {
            write_to(&stream, e.to_string()).unwrap();
            false
        }
    } {}

    println!("closed connection: {:?}", stream.peer_addr());
}

fn get_command(reader: impl Read) -> Result<Command, Box<dyn Error>> {
    let raw_cmd = read_from(reader)?;

    match raw_cmd.as_str() {
        "on" => Ok(Command::On),
        "off" => Ok(Command::Off),
        "info" => Ok(Command::Info),
        "help" => Ok(Command::Help),
        "quit" => Ok(Command::Quit),
        _ => Err(Box::new(CommandError("unknown command".to_string()))),
    }
}

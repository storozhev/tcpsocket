#[derive(Default, Debug)]
pub struct Socket {
    power: u32,
    turned_on: bool,
}

impl Socket {
    pub fn turn_on(&mut self) {
        self.power = 220;
        self.turned_on = true;
    }

    pub fn turn_off(&mut self) {
        self.power = 0;
        self.turned_on = false;
    }
}

use crate::socket::Socket;
use thiserror::Error;

#[derive(PartialEq, Error, Debug)]
#[error("command error: {0}")]
pub struct CommandError(pub String);

#[derive(Debug)]
pub enum Command {
    On,
    Off,
    Info,
    Help,
    Quit,
}

impl Command {
    pub fn execute(&self, socket: &mut Socket) -> String {
        match self {
            Self::On => {
                socket.turn_on();
                "the socket is turned on".to_string()
            }
            Self::Off => {
                socket.turn_off();
                "the socket is turned off".to_string()
            }
            Self::Info => format!("socket info: {:?}", socket),
            Self::Help => commands(),
            Self::Quit => "quit".to_string(),
        }
    }
}

pub fn commands() -> String {
    use Command::*;
    let cmd_list = [On, Off, Info, Help, Quit]
        .iter()
        .map(|c| format!("\n{:?}", c).to_lowercase())
        .collect::<Vec<_>>()
        .join("");

    format!("commands available: {}", cmd_list)
}

#[cfg(test)]
mod test {
    use super::{Command::*, Socket};
    #[test]
    fn command_help() {
        assert!([On, Off, Info, Help, Quit,].into_iter().all(|c| Help
            .execute(&mut Socket::default())
            .contains(&format!("{:?}", c).to_lowercase())));
    }
}
